import React from 'react';
import { NavigationContainer } from '@react-navigation/native';
import { createStackNavigator } from '@react-navigation/stack';
import { createDrawerNavigator } from '@react-navigation/drawer';
import Icon from 'react-native-vector-icons/Ionicons';

import { HomeScreen } from '../screens';
import CustomDrawer from '../components/CustomDrawer'

// const Stack = createStackNavigator();
const Drawer = createDrawerNavigator();

const AppNavigator = () => (
  <NavigationContainer>
    <Drawer.Navigator
        initialRouteName='Home'
        drawerContent={(props) => <CustomDrawer {...props} />}
        screenOptions={{ headerShown: false }}>
        <Drawer.Screen
          name='Home'
          component={HomeScreen}
          options={{
            drawerIcon: ({ color, size }) => (
              <Icon name='md-home' color={color} size={size} />
            )
          }}
        />
        {/* <Drawer.Screen name='Home' component={HomeScreen} /> */}
    </Drawer.Navigator>
  </NavigationContainer>
);

export default AppNavigator;