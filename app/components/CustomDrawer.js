import React from 'react'
import {
    StyleSheet,
    Text,
    View
} from 'react-native';
import {
    DrawerContentScrollView,
    DrawerItemList,
    DrawerItem,
} from '@react-navigation/drawer';
import { useSelector, useDispatch } from 'react-redux';
import { toggleDarkMode, overrideSystemDarkMode } from 'app/store/actions'
import { version } from 'package.json'
import Icon from 'react-native-vector-icons/Feather'
import { AppColors } from 'app/theme'

const CustomDrawer = (props) => {
    const { darkMode } = useSelector(({ data }) => data);
    const dispatch = useDispatch()
    const onToggleDarkMode = () => {
        dispatch(toggleDarkMode(!darkMode))
        dispatch(overrideSystemDarkMode(true))
        console.log('onToggleDarkMode changed to ', {darkMode: !darkMode})
    }

    const getVersion = () => {
        return version
    }

    return (
        <>
            <DrawerContentScrollView
                {...props}
                style={darkMode ? styles.darkSidebar : styles.lightSidebar}
            >
                {/* <Text>{Object.keys(props.state.routeNames).join(', ')}</Text> */}
                {/* <Text>{props.state.routeNames}</Text> */}
                <DrawerItemList
                    {...props}
                    activeTintColor={darkMode ? '#e03e1f' : '#000'}
                    inactiveTintColor={darkMode ? '#eee': '#444'}
                    activeBackgroundColor={darkMode ? '#222': '#eee'}
                    inactiveBackgroundColor={darkMode ? '#222': '#eee'}
                />
                {/* <DrawerItem
                    label={darkMode ? 'Light Mode' : 'Dark Mode'}
                    style={styles.darkModeBtn}
                    icon={({ focused, color, size }) => <Icon color={color} size={size} name={darkMode ? 'sun' : 'moon'} />}
                    activeTintColor={darkMode ? '#000' : '#eee'}
                    inactiveTintColor={darkMode ? '#eee': '#444'}
                    activeBackgroundColor={darkMode ? '#444': '#ccc'}
                    inactiveBackgroundColor={darkMode ? '#222': '#eee'}
                    onPress={() => onToggleDarkMode()}
                /> */}
            </DrawerContentScrollView>
            {/* <Text style={styles.version}>version: {getVersion()}</Text> */}
        </>
    )
}

export default CustomDrawer

const styles = StyleSheet.create({
    darkSidebar: {
        backgroundColor: '#222',
        color: '#eee',
        borderColor: '#aaa',
        // borderRightWidth: 0.5,
    },
    lightSidebar: {
        backgroundColor: '#eee',
        color: '#000',
    },
    darkModeBtn: {
        borderTopWidth: 1,
        borderColor: AppColors.primary,
    },
    version: {
        color: '#fff',
        backgroundColor: AppColors.primary,
        position: 'absolute',
        right: 10,
        bottom: 10,
        width: 130,
        textAlign: 'center',
        paddingVertical: 3,
        borderRadius: 15,
    },
    build: {
        color: '#fff',
        backgroundColor: AppColors.primary,
    },
})
